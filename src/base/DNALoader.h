#ifndef _H_DNA_LOADER_
#define _H_DNA_LOADER_

#include "dnabase.h"
#include "DNAGroup.h"
#include "DNAStorage.h"

#include <nodePath.h>
#include <namable.h>
#include <asyncTaskManager.h>
#include <asyncTask.h>

class EXPCL_DNA DNALoader : public TypedReferenceCount, public Namable
{
    PUBLISHED:
        explicit DNALoader(const std::string &name = "dnaLoader");
        ~DNALoader();

        NodePath load_DNA_file(DNAStorage* store, const Filename& file);
        PT(DNAGroup) load_DNA_file_AI(DNAStorage* store, const Filename& file);
        
        INLINE void set_task_manager(AsyncTaskManager *task_manager);
        INLINE AsyncTaskManager *get_task_manager() const;
        INLINE void set_task_chain(const std::string &task_chain);
        INLINE const std::string &get_task_chain() const;

        BLOCKING INLINE void stop_threads();
        INLINE bool remove(AsyncTask *task);

        PT(AsyncTask) make_async_request(DNAStorage *store,
                                         const Filename &filename);

        INLINE void load_async(AsyncTask *request);

    public:
        static TypeHandle get_class_type() {
            return _type_handle;
        }
        static void init_type() {
            TypedReferenceCount::init_type();
            Namable::init_type();
            register_type(_type_handle, "DNALoader",
                          TypedReferenceCount::get_class_type(),
                          Namable::get_class_type());
        }
        virtual TypeHandle get_type() const {
            return get_class_type();
        }
        virtual TypeHandle force_init_type() {init_type(); return get_class_type();}

    private:
        static TypeHandle _type_handle;


       	void handle_storage_data(DatagramIterator& dgi);
       	void handle_comp_data(DatagramIterator& dgi);
       	void load_DNA_file_base(DNAStorage* store, const Filename& file);

       	DNAStorage* m_cur_store;
       	PT(DNAGroup) m_cur_comp;
        PT(AsyncTaskManager) _task_manager;
        std::string _task_chain;
};

#include "DNALoader.I"

#endif
