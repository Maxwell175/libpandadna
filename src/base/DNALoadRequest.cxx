/**
 * Derived from ModelLoadRequest from
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file DNALoadRequest.cxx
 * @author Maxwell Dreytser
 * @date 2019-02-02
 */

#include "DNALoadRequest.h"
#include "loader.h"
#include "config_pgraph.h"

TypeHandle DNALoadRequest::_type_handle;

/**
 * Create a new DNALoadRequest, and add it to the loader via load_async(),
 * to begin an asynchronous load.
 */
DNALoadRequest::
DNALoadRequest(const std::string &name,
                 const Filename &filename,
                 DNALoader *loader,
                 DNAStorage *storage
              ) :
  AsyncTask(name),
  _filename(filename),
  _loader(loader),
  _storage(storage)
{
}

/**
 * Performs the task: that is, loads the one model.
 */
AsyncTask::DoneStatus DNALoadRequest::
do_task() {
  double delay = async_load_delay;
  if (delay != 0.0) {
    Thread::sleep(delay);
  }

  PT(PandaNode) model = _loader->load_DNA_file(_storage, _filename).node();
  set_result(model);

  // Don't continue the task; we're done.
  return DS_done;
}
