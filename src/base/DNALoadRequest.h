/**
 * Derived from ModelLoadRequest from
 * PANDA 3D SOFTWARE
 * Copyright (c) Carnegie Mellon University.  All rights reserved.
 *
 * All use of this software is subject to the terms of the revised BSD
 * license.  You should have received a copy of this license along
 * with this source code in a file named "LICENSE."
 *
 * @file DNALoadRequest.h
 * @author Maxwell Dreytser
 * @date 2019-02-02
 */

#ifndef DNALOADREQUEST_H
#define DNALOADREQUEST_H

#include <pandabase.h>

#include <asyncTask.h>
#include <filename.h>
#include <loaderOptions.h>
#include <pandaNode.h>
#include <pointerTo.h>
#include <loader.h>
#include <nodePath.h>

#include "DNALoader.h"

/**
 * A class object that manages a single asynchronous model load request.
 * Create a new DNALoadRequest, and add it to the loader via load_async(),
 * to begin an asynchronous load.
 */
class EXPCL_PANDA_PGRAPH DNALoadRequest : public AsyncTask {
public:
  ALLOC_DELETED_CHAIN(DNALoadRequest);

PUBLISHED:
  explicit DNALoadRequest(const std::string &name,
                            const Filename &filename,
                            DNALoader *loader,
                            DNAStorage *storage
                         );

  INLINE const Filename &get_filename() const;
  INLINE DNALoader *get_loader() const;
  INLINE DNAStorage *get_storage() const;

  INLINE bool is_ready() const;
  INLINE NodePath *get_model() const;

  MAKE_PROPERTY(filename, get_filename);
  MAKE_PROPERTY(loader, get_loader);
  MAKE_PROPERTY(storage, get_storage);

protected:
  virtual DoneStatus do_task();

private:
  Filename _filename;
  PT(DNALoader) _loader;
  PT(DNAStorage) _storage;

public:
  static TypeHandle get_class_type() {
    return _type_handle;
  }
  static void init_type() {
    AsyncTask::init_type();
    register_type(_type_handle, "DNALoadRequest",
                  AsyncTask::get_class_type());
    }
  virtual TypeHandle get_type() const {
    return get_class_type();
  }
  virtual TypeHandle force_init_type() {init_type(); return get_class_type();}

private:
  static TypeHandle _type_handle;
};

#include "DNALoadRequest.I"

#endif
